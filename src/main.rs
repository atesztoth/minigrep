use std::{env, process};
use minigrep::{Config, run};

fn main() {
    let config = Config::build(env::args()).unwrap_or_else(|err| {
        println!("Failed to parse arguments! {}", err);
        process::exit(1)
    });

    // Pattern matching and extracting the value.
    if let Err(e) = run(config) {
        println!("Application error: {e}");
        process::exit(1)
    }
}

